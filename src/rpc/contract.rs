// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

use anyhow::Result;
use rocket::{get, http::Status, response::status::Custom, serde::json::Json, State};
use rusqlite::Connection;
use serde::Serialize;
use serde_json::{json, Value};

use crate::db::DBPool;

#[derive(Serialize)]
pub struct ContractCount {
    active: u64,
    ended: u64,
}

fn db_contract_count_all(db: &Connection) -> Result<ContractCount> {
    let active: u64 = db.query_row(
        "SELECT COUNT(*) FROM pool WHERE withdrawn_in_utxo IS NULL",
        [],
        |row| row.get(0),
    )?;

    let ended: u64 = db.query_row(
        "SELECT COUNT(*) FROM pool WHERE withdrawn_in_utxo IS NOT NULL",
        [],
        |row| row.get(0),
    )?;

    Ok(ContractCount { active, ended })
}

fn db_contract_count_by_token(db: &Connection, token_id: &str) -> Result<ContractCount> {
    let active: u64 = db.query_row(
        "SELECT COUNT(*) FROM pool WHERE withdrawn_in_utxo IS NULL AND token_id = ?",
        [token_id],
        |row| row.get(0),
    )?;

    let ended: u64 = db.query_row(
        "SELECT COUNT(*) FROM pool WHERE withdrawn_in_utxo IS NOT NULL AND token_id = ?",
        [token_id],
        |row| row.get(0),
    )?;

    Ok(ContractCount { active, ended })
}

#[get("/contract/count")]
pub fn contract_count_all(conn: &State<DBPool>) -> Result<Json<Value>, Custom<String>> {
    let db = conn
        .get()
        .map_err(|e| Custom(Status::InternalServerError, format!("Error: {}", e)))?;
    let count = db_contract_count_all(&db)
        .map_err(|e| Custom(Status::BadRequest, format!("Error: {}", e)))?;
    Ok(Json(json!(count)))
}

#[get("/contract/count/<token>")]
pub fn contract_count_token(
    token: &str,
    conn: &State<DBPool>,
) -> Result<Json<Value>, Custom<String>> {
    let db = conn
        .get()
        .map_err(|e| Custom(Status::InternalServerError, format!("Error: {}", e)))?;
    let count = db_contract_count_by_token(&db, token)
        .map_err(|e| Custom(Status::BadRequest, format!("Error: {}", e)))?;
    Ok(Json(json!(count)))
}
