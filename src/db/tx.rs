// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html
use std::time::{SystemTime, UNIX_EPOCH};

use anyhow::Result;
use bitcoin_hashes::hex::ToHex;
use bitcoincash::{BlockHash, Txid};
use rusqlite::{params, Connection};

pub fn create_table(conn: &Connection) {
    let tbl = "CREATE TABLE tx (
            txid TEXT PRIMARY KEY,
            blockhash TEXT,
            mtp_timestamp BIGINT,
            first_seen_timestamp BIGINT
        )";

    conn.execute(tbl, []).expect("failed to create tx table");
}

pub fn insert_block_tx(
    conn: &rusqlite::Connection,
    txid: &Txid,
    blockhash: &BlockHash,
    mtp: i64,
) -> Result<()> {
    let sql = "INSERT INTO tx (txid, blockhash, mtp_timestamp)
               VALUES (?1, ?2, ?3)
               ON CONFLICT(txid) DO UPDATE SET
               blockhash = excluded.blockhash,
               mtp_timestamp = excluded.mtp_timestamp";
    conn.execute(sql, params![&txid.to_hex(), &blockhash.to_hex(), &mtp])?;
    Ok(())
}

pub fn insert_mempool_tx(conn: &rusqlite::Connection, txid: &Txid) -> Result<()> {
    let sql = "INSERT INTO tx (txid, first_seen_timestamp)
               VALUES (?1, ?2)
               ON CONFLICT(txid) DO UPDATE SET
               first_seen_timestamp = excluded.first_seen_timestamp";

    let current_timestamp = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs() as i64;

    conn.execute(sql, params![&txid.to_hex(), current_timestamp])?;
    Ok(())
}
