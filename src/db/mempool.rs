// Copyright (C) 2024 Riften Labs AS
//
// This software is licensed under the GNU Affero General Public License (AGPL), version 3.0 or later.
// A copy of the license can be found in the LICENSE file or at https://www.gnu.org/licenses/agpl-3.0.html

use std::collections::HashSet;

use anyhow::Result;
use bitcoin_hashes::hex::{FromHex, ToHex};
use bitcoincash::Txid;
use rusqlite::{params, Connection};

pub fn load_mempool(conn: &Connection) -> Result<HashSet<Txid>> {
    let mut stmt = conn.prepare("SELECT txid FROM tx WHERE blockhash is NULL")?;
    let txid_iter = stmt.query_map([], |row| row.get(0))?;

    let mut txids: HashSet<Txid> = HashSet::new();
    for txid_res in txid_iter {
        let txid_hex: String = txid_res?;
        let txid = Txid::from_hex(&txid_hex).expect("invalid txid in db");
        txids.insert(txid);
    }
    Ok(txids)
}

pub fn delete_mempool_tx(db_tx: &Connection, txid: &Txid) -> Result<bool> {
    let mut stmt = db_tx.prepare("DELETE FROM utxo_spending WHERE txid IN (SELECT txid FROM tx WHERE txid = ? AND blockhash is NULL)")?;
    let mut rows_deleted = stmt.execute(params![txid.to_hex()])?;

    let mut stmt = db_tx.prepare("DELETE FROM utxo_funding WHERE txid IN (SELECT txid FROM tx WHERE txid = ? AND blockhash is NULL)")?;
    rows_deleted += stmt.execute(params![txid.to_hex()])?;

    let mut stmt = db_tx.prepare("DELETE FROM tx WHERE txid = ? AND blockhash is NULL")?;
    rows_deleted += stmt.execute(params![txid.to_hex()])?;

    Ok(rows_deleted != 0)
}
